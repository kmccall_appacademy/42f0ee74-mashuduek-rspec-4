require 'byebug'
class Book
  # TODO: your code goes here!
  def title=(new_title)
    # connectors = ['and', 'or', 'to', 'the', 'in', 'of', 'a', 'an']
    connectors = %w[and or to the in of a an]

    @title = new_title.split.map.with_index do |word, i|
      next word.capitalize if i == 0
      connectors.include?(word) ? word : word.capitalize
    end.join(' ')

  end

  def title
    @title
  end
end
