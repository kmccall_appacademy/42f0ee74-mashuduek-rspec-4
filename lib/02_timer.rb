class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hrs = self.seconds / 60 / 60
    mins = self.seconds / 60 % 60
    secs = self.seconds % 60
    "#{padded(hrs)}:#{padded(mins)}:#{padded(secs)}"
  end

  def padded(secs)
    seconds = (secs % 60).to_s
    if seconds.size < 2
      seconds.split(//).unshift('0').join
    else
      seconds
    end
  end
end
