require 'byebug'
class Temperature
  def initialize(temp)
    # debugger
    @temp = temp
  end

  def in_fahrenheit
    # if @temp.keys.include?(:c)
    #   (@temp.values[0] * 9) / 5.to_f + 32
    # else
    #   @temp.values[0]
    # end
    if @temp[:c]
      (@temp[:c] * 9) / 5.to_f + 32
    else
      @temp[:f]
    end

  end

  def in_celsius
    # if @temp.keys.include?(:f)
    #   (@temp.values[0] - 32) * 5 / 9.to_f
    # else
    #   @temp.values[0]
    # end
    if @temp[:f]
      (@temp[:f] - 32) * 5 / 9.to_f
    else
      @temp[:c]
    end
  end

  def self.from_celsius(degrees)
    # debugger
    Temperature.new(c: degrees)
  end

  def self.from_fahrenheit(degrees)
    Temperature.new(f: degrees)
  end
end

class Celsius < Temperature
  def initialize(degrees)
    @temp = {c: degrees}
  end
end

class Fahrenheit < Temperature
  def initialize(degrees)
    @temp = {f: degrees}
  end
end
