require 'byebug'
class Dictionary

  def initialize
    @our_hash = Hash.new(nil)
  end

  def entries
    @our_hash
  end

  def add(key_val = nil)
    if key_val == key_val.to_s
      @our_hash[key_val] = nil
    else
      keys = key_val.keys
      values = key_val.values
      @our_hash[keys[0]] = values[0]
    end

  end

  def []=(hash, pair)
    key = pair.keys
    value = pair.values
    hash[key[0]] = value[0]
  end

  def keywords
    @our_hash.keys.sort
  end

  def include?(key)
    @our_hash.keys.include?(key)
  end

  def find(word)
    return {} if @our_hash.empty?
    to_return = {}
    @our_hash.each do |k, v|
      to_return[k] = v if k.include?(word)
    end
    to_return
  end

  def printable
    str = ''
    keys = @our_hash.keys.reverse
    values = @our_hash.values.reverse
    keys.each.with_index do |k, idx|
      values.each.with_index do |v, id|
        if idx == id
          str << "[#{k}] \"#{v}\"\n"
        end
      end
    end
    str.strip
  end

end
